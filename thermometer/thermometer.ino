/* v0.1 */
#include "max6675.h"
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27,16,2);

int thermoDO1 = 4;
int thermoCS1 = 5;
int thermoCLK1 = 6;

int thermoDO2 = 8;
int thermoCS2 = 9;
int thermoCLK2 = 10;

float t1;
float t2;

MAX6675 thermocouple1(thermoCLK1, thermoCS1, thermoDO1);

MAX6675 thermocouple2(thermoCLK2, thermoCS2, thermoDO2);
  
void setup() {
  delay(2000);
  Serial.begin(9600);
  
  lcd.init();
  lcd.backlight();
  lcd.setCursor(4,0);
  lcd.print("Electroma");
  lcd.setCursor(3,1);
  lcd.print("Thermometer");
  delay(3000);
  lcd.clear();
}

void loop() {
    t1 = thermocouple1.readCelsius();
    t2 = thermocouple2.readCelsius();
    
    Serial.print(t1);
    //Serial.print("27.25");
    Serial.print(";");
    Serial.print(t2);
    //Serial.print("25.75");
    Serial.print(";");

    lcd.setCursor(0,0);
    lcd.print("Thermo 1:");
    lcd.setCursor(10,0);
    lcd.print(t1);
    lcd.setCursor(15,0);
    lcd.print("C");
    
    lcd.setCursor(0,1);
    lcd.print("Thermo 2:");
    lcd.setCursor(10,1);
    lcd.print(t2);
    lcd.setCursor(15,1);
    lcd.print("C");

    Serial.print("@"); // stop char
    
    delay(3000);
}
