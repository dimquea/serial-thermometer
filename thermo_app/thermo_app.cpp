﻿#include "pch.h"
#include <windows.h>
#include <iostream>
#include <fstream>
#include <ctime>
using namespace std;

HANDLE hSerial;
LPCWSTR sPortName;

int main() {
	//hallo
	printf("TempRead v0.1 | Dmitry Morozov | 2019\n\n");

	//open conf
	setlocale(LC_ALL, "rus");
	char port[5];
	
	ifstream conf("conf.ini");

	if (!conf.is_open()) { // if conf not opened
		printf("Can't find config file.\n");
		ofstream newConf("conf.ini");
		newConf << "COM1;" << endl;
		printf("New conf.ini created. \nPlease close programm, change port name and start programm again.\n");
		system("pause");
		return 0;
	} else {
		conf.getline(port, 5, ';');
		conf.close(); // close conf
	}

	// try open port
	hSerial = CreateFileA(port, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
	if (hSerial == INVALID_HANDLE_VALUE) {
		CloseHandle(hSerial);
		printf("Error: opening port.\n");
	} else {
		printf("Port %s opened.\n", port);
	}

	// read port params
	DCB dcbSerialParams = { 0 };
	dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
	if (!GetCommState(hSerial, &dcbSerialParams)) {
		CloseHandle(hSerial);
		printf("Error: getting state.\n");
	}

	// write params to port
	dcbSerialParams.BaudRate = CBR_9600;
	dcbSerialParams.ByteSize = 8;
	dcbSerialParams.StopBits = 1;
	dcbSerialParams.Parity = NOPARITY;
	if (!SetCommState(hSerial, &dcbSerialParams)) {
		CloseHandle(hSerial);
		printf("Error: setting serial port state.\n");
	}

	// if we have errors
	int Error = GetLastError();
	if (Error != 0) {
		printf("Error code: %i.\n", Error);
		system("pause");
		return 0;
	}

	// find out the time to name the file
	time_t rawtime;
	struct tm timeinfo;
	char exportFileName[24];
	time(&rawtime);
	localtime_s(&timeinfo, &rawtime);

	sprintf_s(exportFileName, "%d.%d.%d-%d-%d-%d.csv", timeinfo.tm_hour, timeinfo.tm_min, timeinfo.tm_sec, timeinfo.tm_mday, (timeinfo.tm_mon + 1), (timeinfo.tm_year + 1900));
	printf("File name: %s\n", exportFileName);

	ofstream dataFile(exportFileName); // create file
	if (!dataFile.is_open()) { // if file not opened
		printf("Can't create file!\n");
		system("pause");
		return 0;
	}
	// add headers
	dataFile 
		<< "Время;Темп 1;Темп 2;Начало: " 
		<< timeinfo.tm_hour << ":" 
		<< timeinfo.tm_min << ":" 
		<< timeinfo.tm_sec << " " 
		<< timeinfo.tm_mday << "." 
		<< (timeinfo.tm_mon + 1) << "." 
		<< (timeinfo.tm_year + 1900) << ";" 
		<< endl;

	/*
	char data[] = "Hello from C++";  // строка для передачи
	DWORD dwSize = sizeof(data);   // размер этой строки
	DWORD dwBytesWritten;    // тут будет количество собственно переданных байт
	BOOL iRet = WriteFile(hSerial, data, dwSize, &dwBytesWritten, NULL);
	*/

	// read data from port
	while (true) {
		DWORD iSize;
		char sReceivedChar;

		char recievedMessage[13] = ""; // str with recieved butes
		int counter = 0; // counter
		bool messageEnded = false;

		while (!messageEnded) { // add chars to str
			ReadFile(hSerial, &sReceivedChar, 1, &iSize, 0);
			if (iSize > 0) {
				// cout << "num: " << counter << " char: " << sReceivedChar << endl; // debug
				if (sReceivedChar != '@') {
					recievedMessage[counter] = sReceivedChar;
				} else {
					messageEnded = true;
				}
				counter++;
			}
		}

		time_t rawtime;
		struct tm timeinfo;
		char exportFileName[24];
		time(&rawtime);
		localtime_s(&timeinfo, &rawtime);

		//cout << recievedMessage << "size: " << sizeof(recievedMessage) << endl; // debug
		dataFile << timeinfo.tm_hour << ":" << timeinfo.tm_min << ":" << timeinfo.tm_sec << ";" << recievedMessage << endl; // write str to file
		cout << timeinfo.tm_hour << ":" << timeinfo.tm_min << ":" << timeinfo.tm_sec << ";" << recievedMessage << endl; // out to console
	}
}